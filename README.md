Analog Plus
===========

Here's a classic pilot analog watch that's pretty easy to read, with a graph of HR heart rate. In the middle are two fields showing step count and day/date.

The orange hand in the small dial at the bottom shows how much battery you have remaining, with 6 o'clock showing 100% full, and 12 o'clock showing 50% full.

Release notes:

v4.0.2 When dark mode is selected, the minute hand is now yellow instead of dark blue

v4.0.1 Now allows you to select between light, dark and sepia watchfaces. Also supports partial update so seconds can show all the time on supported watches.

v4.0.0 Now supports watches with 240 and 260 pixels

v3.0.4 HR graph shows a gap if the watch marks the measurement as unreliable

v3.0.3 HR shows as zero if the watch marks the measurement as unreliable

v3.0.1 battery hand is move visible

v3.0.0 re-released for the Fenix 3HR and 5S

v2.0.1 a new design

This app is open source and you can get the source code from https://gitlab.com/nz_brian/garmin.watch.analogplus