using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Application as App;
using Toybox.Time.Gregorian as Calendar;
using Toybox.Time as Time;
using Toybox.ActivityMonitor as Act;

class AnalogPlusWatchView extends Ui.WatchFace {

	var face;
    var showSeconds = false;
    
    hidden var SCREEN_SIZE = 0;

    hidden var centerX = SCREEN_SIZE / 2;
    hidden var centerY = SCREEN_SIZE / 2;
    
    hidden var textColour = Gfx.COLOR_BLACK;
	hidden var minuteColour = Gfx.COLOR_DK_BLUE;
    
    hidden var heartRateGraph;
    hidden var maxHR = 150.0; //max Hr that can fit on the graph
	hidden var minX, maxY, minY, maxX, numberOfResults, xFactor, yFactor;
	hidden var dateStr, steps;
	hidden var minuteHand, hourHand, batteryHand;
    
    hidden enum {
    	light,
    	dark,
    	sepia
    }
    
    hidden var backgroundColour = light;

    function initialize() {
    	SCREEN_SIZE = System.getDeviceSettings().screenWidth;
    	centerX = SCREEN_SIZE / 2;
    	centerY = SCREEN_SIZE / 2;
    	//System.println("Screen: " + SCREEN_SIZE);
    	//System.println("has onPartialUpdate: " + Toybox.WatchUi.WatchFace has :onPartialUpdate);
    	
    	minX = 0.34 * SCREEN_SIZE;
		maxY = 0.45 * SCREEN_SIZE;
		minY = 0.22 * SCREEN_SIZE;
		maxX = 0.73 * SCREEN_SIZE;
		numberOfResults = (maxX-minX).toNumber();
		xFactor = (maxX - minX) / (numberOfResults.toFloat() - 1.0);
		yFactor = (maxY - minY) / maxHR;
		heartRateGraph = new [numberOfResults];
    	
        WatchFace.initialize();
    }

    //! Load your resources here
    function onLayout(dc) {
    	backgroundColour = getPropertyById("background_colour");
    	//System.println("backgroundColour: " + backgroundColour);
    	
    	switch (backgroundColour) {
    		case light:
    			textColour = Gfx.COLOR_BLACK;
		    	switch (SCREEN_SIZE) {
		        	case 218:
		        		face = Ui.loadResource(Rez.Drawables.light218);
		        		break;
		        		
		    		case 240:
		        		face = Ui.loadResource(Rez.Drawables.light240);	
		        		break;	
		        		
		    		case 260:
		        		face = Ui.loadResource(Rez.Drawables.light260);	
		        		break;	
		        		
		    		default:
		        }
		        break; // light
        
			case dark:
				textColour = Gfx.COLOR_WHITE;
				minuteColour = Gfx.COLOR_YELLOW;
		    	switch (SCREEN_SIZE) {
		        	case 218:
		        		face = Ui.loadResource(Rez.Drawables.dark218);
		        		break;
		        		
		    		case 240:
		        		face = Ui.loadResource(Rez.Drawables.dark240);	
		        		break;	
		        		
		    		case 260:
		        		face = Ui.loadResource(Rez.Drawables.dark260);	
		        		break;	
		        		
		    		default:
		        }
		        break; // dark
     
        	case sepia:
        		textColour = Gfx.COLOR_BLACK;
		    	switch (SCREEN_SIZE) {
		        	case 218:
		        		face = Ui.loadResource(Rez.Drawables.sepia218);
		        		break;
		        		
		    		case 240:
		        		face = Ui.loadResource(Rez.Drawables.sepia240);	
		        		break;	
		        		
		    		case 260:
		        		face = Ui.loadResource(Rez.Drawables.sepia260);	
		        		break;	
		        		
		    		default:
		        }
		        break; // sepia

        }
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
    	//System.println("full update!");
        dc.drawBitmap(0,0,face);

        drawStats(dc, false);
        drawHR(dc, false);
        drawClock(dc, false);
        drawBattery(dc, false);
    }
    
    function onPartialUpdate(dc) {
    	//System.println("partial update!");
    	dc.drawBitmap(0,0,face);
        drawStats(dc, true);
        drawHR(dc, true);
        drawClock(dc, true);
        drawBattery(dc, true);
    }

    function drawHR(dc, isPartial) {
    	
		var hrIterator = Act.getHeartRateHistory(numberOfResults, false);
		var sample = hrIterator.next();

		if (null == heartRateGraph || !isPartial) {
			var hr = 0;
			for (var i = 0; i < numberOfResults; i++) {
				if (null != sample && ActivityMonitor.INVALID_HR_SAMPLE != sample.heartRate) {
					hr = sample.heartRate;
				}
				else {
					hr = 0;
				}
				heartRateGraph[i] = [(i * xFactor).toNumber(), (0 - hr * yFactor).toNumber()];
				sample = hrIterator.next();
			}	
		}

		drawLine(dc, minX, maxY, heartRateGraph, Gfx.COLOR_DK_GREEN);
		
        //dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
		//dc.drawRectangle(minX, minY, maxX - minX, maxY-minY);

    }

    function drawStats(dc, isPartial) {
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.drawRectangle(0.01 * SCREEN_SIZE, 0.45 * SCREEN_SIZE, 0.295 * SCREEN_SIZE, 0.11 * SCREEN_SIZE);
        dc.drawRectangle(0.7 * SCREEN_SIZE, 0.45 * SCREEN_SIZE, 0.295 * SCREEN_SIZE, 0.11 * SCREEN_SIZE);
        
        dc.setColor(textColour, Gfx.COLOR_TRANSPARENT);
        
        if (null == dateStr || !isPartial) {
	        var date = Calendar.info(Time.now(), Time.FORMAT_MEDIUM);
	        dateStr = Lang.format("$1$ $2$", [date.day_of_week, date.day]);
    	}
        dc.drawText(.95*SCREEN_SIZE, SCREEN_SIZE / 2, Gfx.FONT_SMALL, dateStr, Gfx.TEXT_JUSTIFY_RIGHT | Gfx.TEXT_JUSTIFY_VCENTER);
        
    	if (null == steps || !isPartial) {
    		steps = Act.getInfo().steps;
    	}
		dc.drawText(0.04 * SCREEN_SIZE, SCREEN_SIZE / 2, Gfx.FONT_SMALL, steps, Gfx.TEXT_JUSTIFY_LEFT | Gfx.TEXT_JUSTIFY_VCENTER);

    }



    function drawClock(dc, isPartial) {
        var time = Sys.getClockTime();

		if (null == minuteHand || null == hourHand || !isPartial) {
			var minute = time.min.toFloat() + time.sec.toFloat()/60;
			minuteHand = getHand(centerX, centerY, minute, 0.45*SCREEN_SIZE, 0.01*SCREEN_SIZE, 0, false);
			
			var hour = (((time.hour % 12) * 60 + (time.min)).toFloat() / 12);
        	hourHand = getHand(centerX, centerY, hour, 0.3*SCREEN_SIZE, 0.01*SCREEN_SIZE, 0, false);
		}

		drawShadow(dc, minuteHand);
		drawShadow(dc, hourHand);

        dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
        dc.fillPolygon(hourHand);

        dc.setColor(minuteColour, Gfx.COLOR_TRANSPARENT);
        dc.fillPolygon(minuteHand);
        

		if (showSeconds || isPartial) {
        	var secondHand = getHand(centerX, centerY, time.sec, 0.45*SCREEN_SIZE, 0.005*SCREEN_SIZE, -15, false);
			drawShadow(dc, secondHand);
			dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
    		dc.fillPolygon(secondHand);
		}
        dc.fillCircle(centerX, centerY, 0.03 * SCREEN_SIZE);
    }


    function drawBattery(dc, isPartial) {
//        var steps = info.steps;
//        var goal = info.stepGoal;
		if (null == batteryHand || !isPartial) {
        	var battery = Sys.getSystemStats().battery;
        	if (null == battery) { battery = 0; }
        	battery = battery.toNumber();

        	// hour expressed as 0 to 5
        	batteryHand = getHand(centerX, 0.675*SCREEN_SIZE, battery*60.0/100 + 30, 0.105*SCREEN_SIZE, 0.01*SCREEN_SIZE, 0, false);
		}
		
        drawShadow(dc, batteryHand);

        // Draw the polygon
        dc.setColor(Gfx.COLOR_ORANGE, Gfx.COLOR_TRANSPARENT);
        dc.fillPolygon(batteryHand);

        dc.fillCircle(centerX, 0.675*SCREEN_SIZE, 0.01*SCREEN_SIZE);
        

    }


    function drawShadow(dc, points) {

    	var result = new [points.size()];
        for (var i = 0; i < result.size(); i += 1) {
        		result[i] = [points[i][0] + 1, (points[i][1]) + 5];
        }
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.fillPolygon(result);

    }

	// centerX = x coord of circle centre
	// centerY = y coord of "
	// minute = from 0 to 59 representing where to point the hand
	// length = how long the hand will be, in pixels
	// width = how wide the base of the hand will be, in pixels
	// radius = how far from the centre to start the hand. 0 is at the centre. Can be + or -
	// is3Sided = if true, will be a triangle pointing to the minute. If false, will be a rectangle.
    function getHand(centerX, centerY, minute, length, width, radius, is3Sided) {
    		var angle = minute * Math.PI / 30;
        var handShape;
        if (is3Sided) {
        		handShape = [ [-width, -radius], [0, -length], [width, -radius] ];
    		}
    		else {
    			handShape = [ [-width, -radius], [-width, -length], [width, -length], [width, -radius] ];
    		}
        var result = new [handShape.size()];
        var cosAngle = Math.cos(angle);
        var sinAngle = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < result.size(); i += 1)
        {
            var x = (handShape[i][0] * cosAngle) - (handShape[i][1] * sinAngle);
            var y = (handShape[i][0] * sinAngle) + (handShape[i][1] * cosAngle);
            result[i] = [ centerX+x, centerY+y];
        }
        return result;
    }

	// dc.fillPolygon has a 64 point limit
	// will not draw the zero Y segments
    function drawLine(dc, baseX, baseY, polygon, color) {

        var size = polygon.size();

        for (var i = 1; i < size; i++) {
        		if (0 != polygon[i-1][1] && 0 != polygon[i][1]) {
				var x1 = baseX + polygon[i-1][0];
				var y1 = baseY + polygon[i-1][1];
				var x2 = baseX + polygon[i][0];
				var y2 = baseY + polygon[i][1];

	        	drawLineSegment(dc, x1, y1,     x2, y2,     2, color);
				drawLineSegment(dc, x1, y1 + 2, x2, y2 + 2, 1, Gfx.COLOR_LT_GRAY);
			}
        }
    }

    function drawLineSegment(dc, x1, y1, x2, y2, penwidth, color) {
        dc.setPenWidth(penwidth);
		dc.setColor(color, Gfx.COLOR_TRANSPARENT);
    	dc.drawLine(x1, y1, x2, y2);
    }


    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    //! The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
        showSeconds = true;

    }

    //! Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
        showSeconds = false;
    }
    
    function getPropertyById(id) {
        var result = App.getApp().getProperty(id);
        if (null == result) {
            result = 0;
        }
        return result;
    }

}
